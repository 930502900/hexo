title: Jenkins FAQ
date: 2018-06-07 11:30:42
categories:
- 技术
tags:
- Jenkins
---

### Jenkins FAQ

### 1.由于权限设置不对导致Jenkins无法登录

##### 在jenkins默认的主目录.jenkins中修改config.xml文件

```
这个权限对应“任何用户可以做任何事(没有任何限制)

<authorizationStrategy class="hudson.security.AuthorizationStrategy$Unsecured">
</authorizationStrategy>
```

```
这个权限对应“登录用户可以做任何事

<authorizationStrategy class="hudson.security.FullControlOnceLoggedInAuthorizationStrategy">
</authorizationStrategy>
```

```
这个权限对应 test用户可以是管理员、打标签权限

<authorizationStrategy class="hudson.security.GlobalMatrixAuthorizationStrategy">
  <permission>hudson.model.Hudson.Administer:test</permission>
  <permission>hudson.scm.SCM.Tag:test</permission>
</authorizationStrategy>
```
----

### 2.关闭以及重启jenkins

##### > 关闭
##### 点击“系统管理” ->　“准备关机”。
##### “准备关机”的说明：　停止执行新的构建任务以安全关闭计算机。

##### 方法二：

##### 采用 Jenkins CLI的方式来关闭Jenkins [参考笔记： Jenkins 三： Jenkins CLI](http://www.cnblogs.com/miniren/p/5207654.html)
##### 命令是：`java -jar jenkins-cli.jar -s http://localhost:8080/ shutdown`

##### > 重启
##### 采用 Jenkins CLI的方式来重启Jenkins [参考笔记： Jenkins 三： Jenkins CLI](http://www.cnblogs.com/miniren/p/5207654.html)
##### 命令是：`java -jar jenkins-cli.jar -s http://localhost:8080/ restart`

##### [摘自](https://www.cnblogs.com/miniren/p/5212242.html)
----

### 3.自动触发构建（通过 `Generic Webhook Trigger`）
```
http://username:token@host:port/generic-webhook-trigger/invoke?token=`配置中开启“触发远程构建”，设置的token`
```
##### 后面可以拼接自定义参数 `&param=param`
