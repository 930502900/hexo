title: Nginx部署静态页出现中文乱码
date: 2018-06-13 11:30:42
categories:
- 技术
tags:
- Nginx
---

# Nginx部署静态页出现中文乱码

### 在nginx服务器用vim编了个测试HTML网页。。。。浏览器访问乱码，编辑了下nginx的配置文件nginx.conf添加了一行charset utf-8;

```
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;
    charset utf-8;    #对所有主机生效或者添加在service模块只对单个主机生效比如下面#内容
    server {
        listen       80;
        server_name  ***.***.***;
        #charset utf-8;   #"没错就是这里"
        location / {
            root   html/www;
            index  index.html index.htm;
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
```

### !!!!!!重启nginx服务 
### F5刷新了下 。。。。。成功！



