title: hexo部署问题总结
date: 2016-01-21 11:30:42
categories:
- 技术
tags:
- hexo
---

# hexo部署问题总结
----
### hexo部署之后没有标签和分类的主页
### go to your hexo folder

### execute command
```
hexo new page tags
hexo new page categories
```

### modify .../source/tags/index.md  OR  .../source/categories/index.md, add line
```
type: "tags"  OR  type: "categories" 
```

### if you don't want to have comments on that page, also add 
```
comments: false
```
----
### hexo部署失败
```
$ hexo d 
```

```
ERROR Deployer not found: git
```

```
npm install --save hexo-deployer-git
```

### 即可。
