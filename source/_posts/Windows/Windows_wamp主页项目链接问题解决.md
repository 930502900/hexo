title: wamp主页项目链接问题解决
date: 2016-07-19 09:12:42
categories:
- Windows
tags:
- wamp
---

# wamp主页项目链接问题解决

### 打开www目录下的index.php文件，搜索if (is_dir($file) && !in_array($file,$projectsListIgnore)) ，判断逻辑里面的a标签就是目录，在http://后面加上localhost/即可

```
if (is_dir($file) && !in_array($file,$projectsListIgnore))
{       
//[modif oto] Ajout éventuel de http:// pour éviter le niveau localhost dans les url
$projectContents .= '<li><a href="'.($suppress_localhost ? 'http://localhost/' : '').$file.'">'.$file.'</a></li>';
}
```
