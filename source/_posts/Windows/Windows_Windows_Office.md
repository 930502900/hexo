title: Windows_Office
date: 2016-08-09 09:12:42
categories:
- Windows
tags:
- Office
---

# Windows Office

> XP系统最高只能安装2010！

### 简体中文版 Office 2010 pro plus retail 32和64合包

### 安装密钥：`GXVBT-YXFGD-QT97T-VPKWB-TF328`，安装之前要在控制面板把office组件卸载干净

```
ed2k://|file|cn\_office\_professional\_plus\_2010\_x86\_x64\_dvd\_515527.iso|1850281984|8CA2D23BCB767EDEE53C7F7455A60C72|/
```

### 苹果系统的看过来！
### 简体中文版office for mac 2016

```
ed2k://|file|mu\_office\_home\_and\_business\_2016\_for\_mac\_mac\_dvd\_7027756.iso|1214924800|D6FA02597D30709949C4FEA6AA0F9D6B|/
```

### 简体中文版 Office 2016 pro plus retail的32和64合包
### 打开迅雷复制下载即可，安装之前要在控制面板把office组件卸载干净

```
ed2k://|file|cn\_office\_professional\_plus\_2016\_x86\_x64\_dvd\_6969182.iso|2588266496|27EEA4FE4BB13CD0ECCDFC24167F9E01|/
```

### 简体中文版 Office 2013 pro plus retail的32和64合包
### 打开迅雷复制下载即可，安装之前要在控制面板把office组件卸载干净

```
ed2k://|file|cn\_office\_professional\_plus\_2013\_with\_sp1\_x86\_and\_x64\_dvd\_3921921.iso|1838749696|C2C7DCB43293252480A32F91F21DE3B3|/
```

### 简体中文版 Project 2016 pro 32和64合包

```
ed2k://|file|cn\_project\_professional\_2016\_x86\_x64\_dvd\_6966612.iso|2588266496|DEF65A0A9B12D8A8B734528800F625D5|/
```

### 简体中文版 VISIO 2016 32/64位合包

```
ed2k://|file|cn\_visio\_professional\_2016\_x86\_x64\_dvd\_6970929.iso|2588262400|52A997F3AF4E40B896C8E4677CF10E90|/
```

### 安装中文版或英文版，如果中文和英文都用到，可以搞中英双语可切换，但并不是中文版和英文版都安装，这样不行。

### 英文版 Office 2016 pro plus retail的32和64合包

```
ed2k://|file|en\_office\_professional\_plus\_2016\_x86\_x64\_dvd\_6962141.iso|2421989376|1B32860B8D8BF3EF2B1C30F79A766879|/
```

### 英文版 Office 2013 pro plus sp1 retail的32和64合包

```
ed2k://|file|en\_office\_professional\_plus\_2013\_with\_sp1\_x86\_and\_x64\_dvd\_3928186.iso|1626122240|C3D0C9D64D276BC2714260C3D580844B|/
```

### 英文版 Office 2010 pro plus retail的32和64合包，
### 安装密钥：`GXVBT-YXFGD-QT97T-VPKWB-TF328`

```
ed2k://|file|en\_office\_professional\_plus\_2010\_x86\_x64\_dvd\_515529.iso|1532469248|EDFC6282FC2F34E0AE66D3FC8B7ED1A2|/
```

### Office 2016 Pro Plus 64位
### 文件名：SW_DVD5_Office_Professional_Plus_2016_64Bit_ChnSimp_MLF_X20-42426.ISO
### 文件大小： 1123452928 字节
### MD5: 60DC8B1892F611E41140DD3631F39793
### SHA1: AEB58DE1BC97685F8BC6BFB0A614A8EF6903E318
### CRC32: 8D8AC6D1

```
ed2k://|file|SW\_DVD5\_Office\_Professional\_Plus\_2016\_64Bit\_ChnSimp\_MLF\_X20-42426.ISO|1123452928|31087A00FF67D4F5B4CBF4AA07C3433B|/
```

### Office 2016 Pro Plus 32位
### 文件名：SW_DVD5_Office_Professional_Plus_2016_W32_ChnSimp_MLF_X20-41351.ISO
### 文件大小：986441728 字节
### MD5: 49D97BD1B4DFEAAA6B45E3DD3803DAC1
### SHA1: 0218F50774AAB63AF7755B0986CDB9972B853E44
### CRC32: FF96B0B5

```
ed2k://|file|SW\_DVD5\_Office\_Professional\_Plus\_2016\_W32\_ChnSimp\_MLF\_X20-41351.ISO|986441728|2DE74581C10096137481873B3AD57D43|/
```

### Office 2016 Project 专业版 64位版
### 文件名：SW_DVD5_Project_Pro_2016_64Bit_ChnSimp_MLF_X20-42676.ISO
### 文件大小：647157760 字节
### MD5: B872E55B8F4A8791D65BCF1DB46D1DCB
### SHA1: 3C180FDAF91DBD0CB767BD040B42B0599FC53438
### CRC32: 6AB6A570

```
ed2k://|file|SW\_DVD5\_Project\_Pro\_2016\_64Bit\_ChnSimp\_MLF\_X20-42676.ISO|647157760|0BBBF20CA3A5F61A819586ADCE6E4DCB|/
```

### Office 2016 Project 专业版 32位版
### 文件名：SW_DVD5_Visio_Pro_2016_64Bit_ChnSimp_MLF_X20-42759.ISO
### 文件大小：714913792 字节
### MD5: 93BEB874F5A5870D5854519856047103
### SHA1: 71E082174812F748AB1A70CA33E6004E1E1AACA8
### CRC32: F813794B

```
ed2k://|file|SW\_DVD5\_Project\_Pro\_2016\_W32\_ChnSimp\_MLF\_X20-41511.ISO|555210752|CA3BD5F8C7B3E263105B041DDD4104AB|/
```

### Office 2016 Visio 专业版 64位版
### 文件名：SW_DVD5_Visio_Pro_2016_64Bit_ChnSimp_MLF_X20-42759.ISO
### 文件大小：714913792 字节
### MD5: 93BEB874F5A5870D5854519856047103
### SHA1: 71E082174812F748AB1A70CA33E6004E1E1AACA8
### CRC32: F813794B

```
ed2k://|file|SW\_DVD5\_Visio\_Pro\_2016\_64Bit\_ChnSimp\_MLF\_X20-42759.ISO|714913792|FC930AB97B366B3595FC2F28ABAC2A6F|/
```

### Office 2016 Visio 专业版 32位版
### 文件名：SW_DVD5_Visio_Pro_2016_W32_ChnSimp_MLF_X20-41580.ISO
### 文件大小：609447936 字节
### MD5: 96E008B110F308F1E424D11964D82CE0
### SHA1: 780046411EB18874AA2DA7E4A11322557EB00D92
### CRC32: 42E1653D

```
ed2k://|file|SW\_DVD5\_Visio\_Pro\_2016\_W32\_ChnSimp\_MLF\_X20-41580.ISO|609447936|91EB248558F236AA66D234EA03FAD9A9|/
```


