title: Windows_安装序列号
date: 2016-06-20 09:12:42
categories:
- Windows
tags:
- 安装序列号
---

# Windows 安装序列号

### Win8.1安装序列号

#### 核心版（multiple editions）： `334NH-RXG76-64THK-C7CKG-D3VPT`

#### 专业版（Professional）： `XHQ8N-C3MCJ-RQXB6-WCHYG-C9WKB`

### Win10安装序列号

#### Windows 10专业版：`W269N-WFGWX-YVC9B-4J6C9-T83GX`

#### Windows 10专业版N：`MH37W-N47XK-V7XM9-C7227-GCQG9`

#### Windows 10企业版：`NPPR9-FWDCX-D2C8J-H872K-2YT43`

#### Windows 10企业版N：`DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4`

#### Windows 10教育版：`NW6C2-QMPVW-D7KKK-3GKT6-VCFB2`

#### Windows 10教育版N：`2WH4N-8QGBV-H22JP-CT43Q-MDWWJ`

#### Windows 10企业版2015 LTSB：`WNMTR-4C88C-JK8YV-HQ7T2-76DF9`

#### Windows 10企业版2015 LTSB N：`2F77B-TNFGY-69QQF-B8YKP-D69TJ`

### 以下xp系统激活码都是可用的

#### (工行版) 可用(强推此号)：`MRX3F-47B9T-2487J-KWKMF-RPWBY`

#### (台湾交大学生版) 可用：`QC986-27D34-6M3TY-JJXP9-TBGMD`

#### 可用：`CM3HY-26VYW-6JRYC-X66GX-JVY2D` 

#### 可用：`DP7CM-PD6MC-6BKXT-M8JJ6-RPXGJ` 

#### 可装不可升级：`F4297-RCWJP-P482C-YY23Y-XH8W3` 

#### `HH7VV-6P3G9-82TWK-QKJJ3-MXR96`

#### `HCQ9D-TVCWX-X9QRG-J4B2Y-GR2TT`
