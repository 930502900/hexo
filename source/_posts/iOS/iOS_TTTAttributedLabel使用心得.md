title: TTTAttributedLabel使用心得
date: 2017-03-10 05:00:00
categories:
- iOS
tags:
- TTTAttributedLabel
---

# TTTAttributedLabel使用心得

### TTTAttributedLabel实现了在label上添加链接等点击事件的功能
### 点击事件TTT的实现方法是在`- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event`方法中获取到touch的点，随后将这个点传入`- (TTTAttributedLabelLink *)linkAtPoint:(CGPoint)point`方法中，找到点击位置的链接文本，如果有则进入回调，没有则事件结束。
### TTT提供了`- (CFIndex)characterIndexAtPoint:(CGPoint)p`方法来计算、确定点击到的链接文本。

### 方法的实现顺序

- 先判断了一下点击的点是否在label的rect中
- 然后取了全部文字在label上的rect
- 转换坐标系，以便于使用coreText
- 使用了CGPathCreateMutable()创建了一个可变路径
- 用链接文本的区域在label上添加了一个矩形路径
- 获取矩形路径范围内制定长度的字符串的的CTFrame
- 通过CTFrame获取CFArray，获取区域内文字的行数
- 遍历每一行,获取每一行的矩形坐标，与当前点击位置做比较，判断是否点在文字范围内，如果在范围内则再判断是范围内那个字符串
