title: Cocoapods安装与使用
date: 2016-01-25 09:12:42
categories:
- iOS
tags:
- Cocoapods
---

# Cocoapods安装与使用

### 本文将介绍CocoaPods从安装到使用过程。
### CocoaPods是一个负责管理iOS项目中第三方开源代码的工具。使用CocoaPods可以为我们节省设置和更新第三方开源库的时间。 下面将分为两部分来### 讲述如何安装和使用CocoaPods。

### 一、安装CocoaPods
#### 先使用下面命令升级Ruby版本：

```
gem update --system
sudo gem update -n /usr/local/bin --system(更新到新 Mac系统后pod的安装路径变为这个)
```

#### 国内ruby源更新为 `http://gems.ruby-china.org`

#### 更新命令

```
gem source --remove https://gems.ruby-china.org
gem source -a http://gems.ruby-china.org
```

#### 等待更新完成后，然后通过gem命令来下载安装CocoaPods：

```
gem install cocoapods
sudo gem install -n /usr/local/bin cocoapods(更新到新 Mac系统后pod的安装路径变为这个)
sudo gem install -n /usr/local/bin cocoapods --pre(安装最新测试版)
```

#### 安装完毕后调用下面命令，进行CocoaPods设置：

```
pod setup
```

#### 执行完成上述命令后，表示安装成功！下面将讲述如何使用CocoaPods。

### 二、使用CocoaPods
#### 打开终端，使用cd命令定位到需要使用CocoaPods的项目根路径。如：

```
cd /Users/username/Documents/Projects/Demo
```

#### 先考虑项目中需要引用哪些库，然后在通过search命令来查找库的信息，如需要加入JSONKit，那可以如下写法：

```
pod search JSONKit
```

#### 命令执行后会得到如下结果：

```
-> JSONKit (1.5pre)
A Very High Performance Objective-C JSON Library.
pod 'JSONKit', '~> 1.5pre'
- Homepage: https://github.com/johnezang/JSONKit
- Source:   https://github.com/johnezang/JSONKit.git
- Versions: 1.5pre, 1.4 [master repo]
```

#### 其中的`pod ‘JSONKit’, ‘~> 1.5pre’`是待会要写入到Podfile中的信息。如果已经知道这些信息，那可以直接省略此步。直接进行下面的步骤。
#### 得到必要的库信息后，现在需要在项目中建立Podfile文件。通过下面的命令：

```
touch Podfile
```

#### 然后编辑Podfile文件，命令如下：

```
open -e Podfile
```

#### 在弹出的编辑界面中输入下面内容：

```
platform :ios
pod 'JSONKit', '~> 1.5pre'
```

#### 然后调用下面命令来进行初始化：

```
pod install
```

#### 运行完毕后，原本的项目目录会多出一些文件。如图：

![](http://7xqedu.com1.z0.glb.clouddn.com/YmxvZ18%3DMTYwMTI1MDE%3D.png_s)

#### 多了*.xcworkspace pod等文件。这是pod生成的项目管理文件，打开LibDemo.xcworkspace 项目文件，在Xcode里看到是这样的目录结构：

![](http://7xqedu.com1.z0.glb.clouddn.com/YmxvZ18%3DMTYwMTI1MDI%3D.png_s)

#### 这里要注意的一点是，如果不调用创建Podfile命令就直接调用`pod install`命令。将会提示

```
[!] No 'Podfile' found in the current working directory.
```

#### 如果Podfile中添加了新库，可以使用下面命令进行更新：

```
pod update
```

> 现在，你的所有第三方库都已经下载完成并且设置好了编译参数和依赖，你只需要记住如下2点即可：

> 1 使用CocoaPods生成的 .xcworkspace 文件来打开工程，而不是以前的 .xcodeproj 文件。
> 2 每次更改了Podfile文件，你需要重新执行一次`pod install`或者`pod update`命令 。

#### cocoapods至此配置完毕，但是你会发现你并不能在项目中找到你导入的库，你需要在`Build Settings->Search Header`的最后一项中添加` $(PODS_ROOT)`,后面选择`recursive`就可以了。

![](http://7xqedu.com1.z0.glb.clouddn.com/YmxvZ18%3DMTYwMTI1MDM%3D.png_s)

#### 如果`pod setup`太慢，可以直接

```
git clone https://github.com/CocoaPods/Specs.git
```

#### clone成功之后将文件夹外层改名为master，移动至～/.cocoapods/repos/
#### 如果`pod search`失败，可执行

```
rm ~/Library/Caches/CocoaPods/search_index.json
```

#### 然后再尝试`pod search`
