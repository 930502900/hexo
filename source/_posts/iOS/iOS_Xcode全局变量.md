title: Xcode全局变量
date: 2017-06-01 05:00:00
categories:
- iOS
tags:
- Xcode
---

# Xcode全局变量

[原文地址](http://www.2cto.com/kf/201302/191830.html)

### `$(BUILT_PRODUCTS_DIR)`
### build成功后的，最终产品路径－－即`build/Debug-iphoneos`路径下
### `$(TARGET_NAME)`
### 标工程名称
### `$(SRCROOT)`
### 工程文件（比如Nuno.xcodeproj）的路径
### `(CURRENT_PROJECT_VERSION)`
### 当前工程版本号

### 当编译静态库，设备选模拟器(iPhone 5.0 Simulator)，未设置任何Build Settings参数时，默认的基础路径：

### `/Users/xxx/Library/Developer/Xcode/DerivedData/xxxWorkspace-caepeadwrerdcrftijaolkkagbjf`
### 下面用$()代替上面一长串东东
### `$(SYMROOT) = $()/Build/Products`
### `$(BUILD_DIR) = $()/Build/Products`
### `$(BUILD_ROOT) = $()/Build/Products`

### 这三个变量中的$()不会随着Build Settings参数的设置而改变。相反，以下可以通过设置而改变
### `$(CONFIGURATION_BUILD_DIR) = $()/Build/Products/Debug-iphonesimulator`
### `$(BUILT_PRODUCTS_DIR) = $()/Build/Products/Debug-iphonesimulator`
### `$(CONFIGURATION_TEMP_DIR) = $()/Build/Intermediates/UtilLib.build/Debug-iphonesimulator`
### `$(TARGET_BUILD_DIR) = $()/Build/Products/Debug-iphonesimulator`
### `$(SDK_NAME) = iphonesimulator5.0`
### `$(PLATFORM_NAME) = iphonesimulator`
### `$(CONFIGURATION) = Debug`
### `$(TARGET_NAME) = UtilLib`
### `$(EXECUTABLE_NAME) = libUtilLib.a` 可执行文件名
### `${IPHONEOS_DEPLOYMENT_TARGET} 5.0`
### `$(ACTION) = build`
### `$(CURRENTCONFIG_SIMULATOR_DIR)` 当前模拟器路径
### `$(CURRENTCONFIG_DEVICE_DIR)` 当前设备路径

### `$(BUILD_DIR)/$(CONFIGURATION)$(EFFECTIVE_PLATFORM_NAME =`

### `$()/Build/Products/Debug-iphonesimulator`

### `$(PROJECT_TEMP_DIR)/$(CONFIGURATION)$(EFFECTIVE_PLATFORM_NAME) = $()/Build/Intermediates/UtilLib.build/Debug-iphonesimulator`

### 自定义变量
### `${CONFIGURATION}-iphoneos` 表示：Debug-iphoneos
### `${CONFIGURATION}-iphonesimulator` 表示：Debug-iphonesimulator
### `$(CURRENTCONFIG_DEVICE_DIR) = ${SYMROOT}/${CONFIGURATION}-iphoneos`
### `$(CURRENTCONFIG_SIMULATOR_DIR) = ${SYMROOT}/${CONFIGURATION}-iphonesimulator`

### 自定义一个设备无关的路径（用来存放各种架构arm6/arm7/i386输出的产品）
### `$(CREATING_UNIVERSAL_DIR) = ${SYMROOT}/${CONFIGURATION}-universal`

### 自定义变量代表的值
### `$(CURRENTCONFIG_DEVICE_DIR) ＝ $()/Build/Products/Debug-iphoneos`
### `$(CURRENTCONFIG_SIMULATOR_DIR) = $()/Build/Products/Debug-iphonesimulator`
### `$(CREATING_UNIVERSAL_DIR) = $()/Build/Products/Debug-universal`

