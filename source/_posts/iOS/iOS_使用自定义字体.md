title: 使用自定义字体
date: 2016-02-18 05:00:00
categories:
- iOS
tags:
- 字体
---

# 使用自定义字体

#### 下载字体资源文件（.ttf或.otf格式的文件）
#### 添加对应的字体(.ttf或.otf格式的文件)到工程的resource
#### 在`info.plist`中添加一项 `Fonts provided by application` (item0对应的value为simkai.ttf，添加多个字体依次添加就可以了)

![](http://7xqedu.com1.z0.glb.clouddn.com/YmxvZ18%3DMTYwMjE4MDE%3D.png_s)

#### 在代码中使用自定义字体
#### 使用示例如下：

```
UITextView *msg = [[UITextView alloc] init];
msg.font = [UIFont fontWithName:@"MicrosoftYaHei" size:18.0f];//MicrosoftYaHei为字体的名称，此处为微软雅黑字体
```
