title: CABasicAnimation
date: 2016-08-23 05:00:00
categories:
- iOS
tags:
- CABasicAnimation
---

# CABasicAnimation

### 今天用到了CABasicAnimation来处理一个动画，简单学习一下

### AnimationWithKeyPath的值

```
transform.scale = 比例轉換
transform.scale.x = 闊的比例轉換
transform.scale.y = 高的比例轉換
transform.rotation.z = 平面圖的旋轉
opacity = 透明度
margin
zPosition
backgroundColor 背景颜色
cornerRadius 圆角
borderWidth
bounds
contents
contentsRect
cornerRadius
frame
hidden
mask
masksToBounds
opacity
position
shadowColor
shadowOffset
shadowOpacity
shadowRadius
```
