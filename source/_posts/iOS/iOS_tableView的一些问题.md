title: tableView的一些问题
date: 2017-08-01 05:00:00
categories:
- iOS
tags:
- tableView
---

# tableView的一些问题

### 使用tableView时实现分组效果会遇到sectionHeader是否停留的处理，停留就选择plain，不停留就选择group

### 使用group时会遇到分组下边多出一块，那是默认的heightForFooterInSection，在代理方法里返回0.01即可
