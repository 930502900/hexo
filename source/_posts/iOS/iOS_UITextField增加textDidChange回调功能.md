title: UITextField增加textDidChange回调功能
date: 2016-01-22 05:00:00
categories:
- iOS
tags:
- UITextField
---

# UITextField增加textDidChange回调功能

### 在使用UITextField来判断登陆按钮状态时只有shouldChangeCharactersInRange函数,是在文件还没有改变前就调用了,而不是在改变后调用,要想实现改变后调用的功能,导致登陆按钮显示状态不准确，我们可以增加事件监听的方式
### 先来看看objective-c提供的接口:

> // add target/action for particular event. you can call this multiple times and you can specify multiple target/actions for a particular event.
> // passing in nil as the target goes up the responder chain. The action may optionally include the sender and the event in that order
> // the action cannot be NULL.

```
- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;
```

### 使用方法：

#### 第一步,对组件增加监听器

```
[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
...
```

#### 第二步,实现回调函数

```
- (void) textFieldDidChange:(id) sender {
  UITextField *_field = (UITextField *)sender;
  NSLog(@"%@",[_field text]);
}
```

