title: UIAlertController选项字体修改
date: 2016-05-17 05:00:00
categories:
- iOS
tags:
- UIAlertController
---

# UIAlertController选项字体修改

```
_addMateAlert = [UIAlertController alertControllerWithTitle:@"快加入你的另一半吧" message:@"在此功能内，你的另一半可以提醒你去完成计划，在你计划完成到一定阶段时你还可以向他许愿，以资鼓励～" preferredStyle: UIAlertControllerStyleAlert];
UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"以后提醒我" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    NSLog(@"以后提醒我～");
}];
UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"邀请你的另一半" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    NSLog(@"邀请你的另一半");
}];

[cancelAction setValue:[UIColor cyanColor] forKey:@"titleTextColor"];
[deleteAction setValue:kSetColor_Hex(@"ff134a") forKey:@"titleTextColor"];
[deleteAction setValue:[UIColor yellowColor] forKey:@"titleTextColor"];

[_addMateAlert addAction:cancelAction];
[_addMateAlert addAction:deleteAction];
```
