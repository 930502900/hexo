title: Reactive Cocoa
date: 2017-03-28 05:00:00
categories:
- iOS
tags:
- Reactive Cocoa
---

# Reactive Cocoa

### Reactive Cocoa常见类介绍

### RACSiganl
### 信号类,一般表示将来有数据传递，只要有数据改变，信号内部接收到数据，就会马上发出数据

> 注意：
> 信号类(RACSiganl)，只是表示当数据改变时，信号内部会发出数据，它本身不具备发送信号的能力，而是交给内部一个订阅者去发出。
> 默认一个信号都是冷信号，也就是值改变了，也不会触发，只有订阅了这个信号，这个信号才会变为热信号，值改变了才会触发

### 如何订阅信号：调用信号RACSignal的subscribeNext就能订阅
### RACSubscriber
### 表示订阅者的意思，用于发送信号，这是一个协议，不是一个类，只要遵守这个协议，并且实现方法才能成为订阅者。通过create创建的信号，都有一个订阅者，帮助他发送数据


### RACSubject
### 用于取消订阅或者清理资源，当信号发送完成或者发送错误的时候，就会自动触发它

> 使用场景
> 不想监听某个信号时，可以通过它主动取消订阅信号

### RACSubject
### RACSubject信号提供者，自己可以充当信号，又能发送信号

> 使用场景
> 通常用来代替代理，有了它，就不必要定义代理了

### RACTuple
### 重复提供信号类，RACSubject的子类

### RACReplaySubject与RACSubject区别:
### RACReplaySubject可以先发送信号，在订阅信号，RACSubject就不可以

> 使用场景一
> 如果一个信号每被订阅一次，就需要把之前的值重复发送一遍，使用重复提供信号类

> 使用场景二
> 可以设置capacity数量来限制缓存的value的数量,即只缓充最新的几个值

### RACTuple
### 元组类,类似NSArray,用来包装值
### RAC中的集合类，用于代替NSArray,NSDictionary,可以使用它来快速遍历数组和字典

### RACCommand
### RAC中用于处理事件的类，可以把事件如何处理,事件中的数据如何传递，包装到这个类中，他可以很方便的监控事件的执行过程

> 使用场景
> 监听按钮点击，网络请求


RACMulticastConnection通过RACSignal的-publish或者-muticast:方法创建
### RACScheduler
### 用于当一个信号，被多次订阅时，为了保证创建信号时，避免多次调用创建信号中的block，造成副作用，可以使用这个类处理

> 使用注意:
> RACMulticastConnection通过RACSignal的-publish或者-muticast:方法创建

### RACScheduler
### RAC中的队列，用GCD封装的

### RACEvent
### 把数据包装成信号事件(signal event)。它主要通过RACSignal的-materialize来使用，然并卵




