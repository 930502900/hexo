title: UIButton切换selected状态标题闪烁问题
date: 2017-03-10 05:00:00
categories:
- iOS
tags:
- UIButton
---

# UIButton切换selected状态标题闪烁问题

### 在使用UIButton时会遇到切换selected状态然后更换标题，这时候标题会闪，因此只修改normal状态的文字就好了，像这样

```
WS(weakSelf);
[self.joinButton addAction:^(UIButton *button) {
weakSelf.callBack(weakSelf.data,weakSelf.joinButton.isSelected);
[weakSelf.joinButton setSelected:!weakSelf.joinButton.isSelected];
}];
[RACObserve(self.joinButton, selected) subscribeNext:^(id x) {
[weakSelf.joinButton setTitleWithoutAnimate:![x boolValue] ? @"加入" : @"已加入" forState:UIControlStateNormal];
[weakSelf.joinButton setBackgroundColor:[x boolValue] ? kSetColor_Hex(@"C9C9C9") : kColorBlue];
}];
```
