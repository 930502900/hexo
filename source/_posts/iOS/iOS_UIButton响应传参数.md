title: UIButton响应传参数
date: 2016-01-21 05:00:00
categories:
- iOS
tags:
- UIButton
---

# UIButton响应传参数

### 开发过程中，有些初学者经常会问，如何调用UIButton的响应时传入一个参数，但是，

```Objective-C
- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;
```

### 方法是无法传参数的，能得到的只是响应的UIButton。下面我们来学习一下如何通过UIButton来“传参数”。 我们以UITableView 为例，在UITableViewCell中定义一个cell，我们称之为CustomCell，cell上加有一个UIButton的控件。我们要做的是 如何在点击UIButton时获得cell。可以通过以下两种方法实现。

### 使用delegate 首先，在CustomCell类文件中定义protocol，有如下定义

```Objective-C
@protocol CustomCellProtocol
- (void)customCell:(CustomCell *)cell didTapButton:(UIButton *)button;
@end
```

### 将UIButton的响应加在CustomCell文件中，比如这个响应叫做

```Objective-C
- (IBAction)buttonTarget:(id)sender;
```

### 那么在点击button时，就会调用这个方法。这个方法可以实现如下，

```Objective-C
- (IBAction)buttonTarget:(id)sender {
    if ([self.delegate respondsToSelector:@selector(customCell:didTapButton:)]) {
        [self.delegate performSelector:@selector(customCell:didTapButton:) withObject:self   withObject:self.button];
    }
}
```

### 然后在UIViewController里实现代理方法，这样，就能获得这个UIButton所在的UITableViewCell。

### 在ViewController中添加响应 第二种方法时在

```Objective-C
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
```

### 方法中直接给UIButton添加响应。实现如下，

```Objective-C
-(UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomCell *cell = [atableView         dequeueReusableCellWithIdentifier:@"CustomCell"];
    [cell.button addTarget:self action:@selector(didTapButton:)   forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
```

### 那么点击button时就会调用

```Objective-C
- (void)didTapButton:(UIButton *)sender
```
### 剩下的就是如何通过这个sender获得它所在的cell。我们这么实现这个方法，

```Objective-C
- (void)didTapButton:(UIButton *)sender {
    CGRect buttonRect = sender.frame;
    for (CustomCell *cell in [tableView visibleCells]) {
        if (CGRectIntersectsRect(buttonRect, cell.frame)) {
            //cell就是所要获得的
        }
    }
}
```


[原文链接](http://innovatence.com/?p=196)
