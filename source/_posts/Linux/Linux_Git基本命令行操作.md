title: Git基本命令行操作
date: 2016-01-21 05:00:00
categories:
- Linux
tags:
- git
---

# Git基本命令行操作

### A. 新建Git仓库，创建新文件夹

```
git init
```

### B. 添加文件到git索引

```
git add <filename>  --- 单个文件添加
git add *　　--- 全部文件添加
```

### C. 提交到本地仓库

```
git commit -m "代码提交描述"
```

### D. 提交到远端仓库

```
git push origin master
```

### master可以换成你想要推送的任何分支

> 分支：

### 创建一个叫做”lee”的分支，并切换过去

```
git checkout -b lee
```

### 切换回主分支

```
git checkout master
```

### 把新建的分支删除

```
git branch -d lee
```

### 再push分支到远端仓库前，该分支不被人所见到

```
git push origin <branch>
```

> 更新与合并:

### A. 更新本地仓库

```
git pull
```

### B. 自动合并分支，多时引起冲突，冲突后需要手动解决

```
git merge <branch>
```

### C. 合并后需要添加

```
git add <branch>
```

### D. 合并前建议使用对比工具

```
git diff <source_branch> <target_branch>
```

### E. 软件发布是创建标签，标签与标记需要唯一

#### E.1 获取提交ID

```
git log
```

#### E.2 创建标签

```
git  tag  1.2.3  提交ID
```

### F. 回退到某个历史版本

#### F.1 获取提交ID

```
git log
```

#### F.2 回退到指定版本

```
git reset --hard 提交ID
```

### G. 使用reset命令后log是得不到充分信息的，这时我们需要使用reflog，然后再reset

```
git reflog
```

### H. 彩色git输出

```
git config color.ui true
```

### I. 查看远程分支与本地分支

```
git branch -a
```

### J. push一个指定分支名到远程分支，如果远程服务器没有这个分支则创建

```
git push origin <brancheName>
```

### K. 删除一个远程分支

```
git push origin --delete <branchName>
```

### L. 如果使用rm误删了文件，可以通过两步恢复

```
git reset HRAD 文件名
git checkout -- 文件名
```

### M. 删除文件

```
git rm 文件名    （同时删除工作目录与本地仓库的文件）
git rm --cached 文件名     （删除本地仓库文件，并不影响工作目录）
```

### N. 改变上传地址

```
git remote set-url origin ssh://git@git.sailor.cn/~/WeiYu
```

### O. 根据服务器的地址创建本地git与服务器的地址关联

```
git remote add origin ssh://lht@git_server/var/lib/scm/git/lht/test.git
```
