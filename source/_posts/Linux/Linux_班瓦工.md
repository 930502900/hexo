title: 班瓦工
date: 2016-05-22 11:30:42
categories:
- Linux
tags:
- 班瓦工
---

# 班瓦工

### 介绍一个scp吧
### 首先在你的服务器新建目录`／root/.ssh`
### 然后执行

```
scp -P ssh端口号 .ssh/id_rsa.pub 用户@IP地址:/root/.ssh/authorized_keys
```

### 然后就可以不用密码远程连接啦
### 获取pip安装包

```
wget https://bootstrap.pypa.io/get-pip.py --no-check-certificate
```

### 然后执行`python get-pip.py` ,`pip install shadowsocks`
