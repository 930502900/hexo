title: 终端彩色显示
date: 2016-02-08 11:30:42
categories:
- Linux
tags:
- 终端
---

# 终端彩色显示

### 修改.bash_profile文件

```
#enables colorin the terminal bash shell export
#sets up thecolor scheme for list export
#sets up theprompt color (currently a green similar to linux terminal)
#enables colorfor iTerm

export CLICOLOR=1
export LSCOLORS=gxfxcxdxbxegedabagacad
export PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]\$ '
export TERM=xterm-color
alias 自定义命令 = 原命令
unalias 自定义命令
```

### `source xxx`和`. xxx`是在当前shell环境中执行`xxx`文件中的内容，而`sh xxx`或`./xxx`则开启子进程（子shell）执行，故要使`.bash_profile`中的修改立即生效，只能使用`source .bash_profile` 和`. .bash_profile`。
