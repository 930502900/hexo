title: SVN基本命令行操作
date: 2017-07-21 11:30:42
categories:
- Linux
tags:
- SVN
---

# SVN基本命令行操作

### 先复习一下常用的几个命令
### `ls`, `rm`, `git add`, `git commit -a -m 'information'`, `git pull`, `git push`, `git clone`

### add全部问号的文件:`svn status | grep ? |cut -d" " -f2- | xargs svn add`

### 查看状态:`svn st`

### 提交:`svn ci -m 'information'`

### 查看仓库信息:`svn info`

### 切换远程仓库地址:`svn sw --relocate svn://user@serverHost1/docunments svn://user@serverHost2/docunments`

```
user@localhost:~/Desktop/$ svn st
M       Root.ios.js
M       conference/detail.js
M       conference/main.js
?       movies
user@localhost:~/Desktop/$ svn add movies/
A         movies
A         movies/moviesData.js
A         movies/item.js
A         movies/test.js
A         movies/movies.js
A         movies/moviesDetail.js
user@localhost:~/Desktop/$ svn st
M       Root.ios.js
M       conference/detail.js
M       conference/main.js
A       movies
A       movies/item.js
A       movies/movies.js
A       movies/moviesData.js
A       movies/moviesDetail.js
A       movies/test.js
user@localhost:~/Desktop/$ svn ci -m "information"
Sending        Root.ios.js
Sending        conference/detail.js
Sending        conference/main.js
Adding         movies
Adding         movies/item.js
Adding         movies/movies.js
Adding         movies/moviesData.js
Adding         movies/moviesDetail.js
Adding         movies/test.js
Transmitting file data ........
Committed revision 17363.
user@localhost:~/Desktop/$
```
