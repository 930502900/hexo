title: 好用的命令_screen
date: 2016-06-11 09:12:42
categories:
- Linux
tags:
- screen
---

# 好用的命令_screen

### 很多时候大家都是通过ssh远程操作服务器，当你上传一个超大的文件或者开启某个服务的时候，你会发现，你无法关闭你本地的终端，否则一切都会消失……
### 因此，我们需要关闭本地终端还不会因为SIGNUP关掉我们希望一直运行的程序。我为大家推荐一个好用的应用：screen。流行的Linux发行版（例如Red Hat Enterprise Linux 4）通常会自带screen实用程序，如果没有的话，可以从GNU screen的官方网站下载。
### 你可以输入`screen`,这样会打开一个新的终端界面，或者，在你的命令之前加上`screen`，例如`screen vi new.txt`，然后你可以在里面进行操作，当你需要中断会话的时候，你只需要输入`control+a d`就可以了。
### 当你希望恢复中断的会话时，你只需要输入`screen -ls`，这样你的终端就会弹出类似这样一个对话：

```
[root@localhost ~]# screen -ls
There are screens on:
11416.pts-0.localhost	(Detached)
11385.pts-0.localhost	(Detached)
2 Sockets in /var/run/screen/S-root.
```

### 然后选择你要恢复的会话，执行类似screen -r 11385的指令，就可以恢复会话了。
