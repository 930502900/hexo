title: 防火墙Firewalld
date: 2018-06-10 09:12:42
categories:
- Linux
tags:
- Firewalld
---

# 防火墙Firewalld

### 最近想把自己的服务器提升一下档次，以前都是直接关掉防火墙，最经重新搭服务器就想试试防火墙怎么用了

```
[root@centos-tokyo ~]# firewall-cmd --zone=dmz --list-ports

[root@centos-tokyo ~]# firewall-cmd --zone=public --add-port=80/tcp --permanent
success
[root@centos-tokyo ~]# firewall-cmd --zone=dmz --list-ports

[root@centos-tokyo ~]# firewall-cmd --zone=public --list-ports

[root@centos-tokyo ~]# systemctl restart firewalld
[root@centos-tokyo ~]# firewall-cmd --zone=public --list-ports
80/tcp
```

[Centos 7防火墙firewalld开放80端口](https://www.cnblogs.com/hantianwei/p/5736278.html)
[CentOS7下Firewall一些基本常用命令](https://blog.csdn.net/u012486840/article/details/52635263)

