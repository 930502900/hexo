title: 小脚本解决大问题
date: 2016-02-18 11:30:42
categories:
- Linux
tags:
- 脚本
---

# 小脚本解决大问题

#### 就在写上一篇博客的时候，由于我的这个模版不是很完善，所以上传到服务器之后还需要自己处理一些事，最近心情不好，一下子就犯懒了，突然想到了脚本这个东西，我一下精神起来，捅咕了一会总算是好用了，上传到服务器，运行一个脚本，就全都搞定了。后来更懒了，直接把本地博客生成也写成了脚本，灰常棒…
#### 下面说一下这玩意咋写。

#### 1.打开终端,并使用vim新建文件todo.sh

```
vim todo.sh (i)
```

#### 2.输入以下代码

```
#! /bin/bash
mkdir newFolder
```

#### 3.`chmod 777 todo.sh`

#### 4.`./todo.sh`

#### 5.ok
