title: 考研之路20191121
date: 2019-11-21 08:24:16
categories:
- 2019
tags:
- doctornote
---

### 自己挺矛盾的，贪玩的本性改不了，这一年就这么荒废了，不给自己找任何的理由，失去的就要自己拿回来，今年试试运气，明年看看实力！
### 这一次绝对不断更！就算花了大量时间玩游戏也绝对不会断掉任何一天学习的脚步，哪怕只有一点点！

### 今日学习计划

|科目|内容|完成情况（√/×）|
|:---|:---|:---:|
|数学-高等数学|第二章：导数与微分|×|
|数据结构|二叉树的遍历与线索二叉树|×|
|政治|五套卷（三） 以及 错题回顾|√×|
|英语|背单词 以及 阅读理解|××|



[MD语法入门](https://www.jianshu.com/p/399e5a3c7cc5 "MD语法入门")
