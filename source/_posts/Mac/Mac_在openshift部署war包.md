title: 在openshift部署war包
date: 2017-05-02 09:12:42
categories:
- Mac
tags:
- openshift
---

# 在Openshift部署war包

### 获取到项目的ssh链接，使用scp ROOT.war ssh链接:/tmp 将war包上传至openshift
### 通过ssh连接到openshift，`cd jbossews/webapps && mv /tmp/ROOT.war ROOT.war`
### 然后到openshift应用主页restart application就可以了

```
scp ROOT.war 58e5a9f889f5cf3f5f000185@jbossews-0513.rhcloud.com:/tmp
ssh 58e5a9f889f5cf3f5f000185@jbossews-0513.rhcloud.com
```
