title: 查看隐藏文件
date: 2016-01-24 09:12:42
categories:
- Mac
tags:
- 隐藏文件
---

# 查看隐藏文件

### 有一些人（比如笔者）总是喜欢捣鼓 Mac 上面的任何文件，尤其是那些被隐藏起来的。所以，我总是喜欢将系统默认的隐藏文件显示出来。熟悉 Windows 系统的用户可能只需要在文件选项菜单那里打上几个勾就可以做到了，那么对于 Mac 用户呢？

### 如何让 Finder 显示隐藏文件和文件夹

### 第一步：打开「终端」应用程序。
### 第二步：输入如下命令：

```
defaults write com.apple.finder AppleShowAllFiles -boolean true ; killall Finder
```

### 第三步：按下「Return」键确认。
### 现在你将会在 Finder 窗口中看到那些隐藏的文件和文件夹了。
### 如果你想再次隐藏原本的隐藏文件和文件夹的话，将上述命令替换成

```
defaults write com.apple.finder AppleShowAllFiles -boolean false ; killall Finder
```

### 即可。

