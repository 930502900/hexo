title: 一个电脑多个ssh-key
date: 2017-05-11 09:12:42
categories:
- Mac
tags:
- ssh-key
---

# 一个电脑多个ssh-key

### 一个电脑多个ssh-key可以通过config文件来管理

```
Host aaa
HostName github.com
User git
IdentityFile ~/.ssh/id_rsa_aaa

Host bbb
HostName github.com
User git
IdentityFile ~/.ssh/id_rsa_bbb
```

### 配置完成即可通过ssh -T aaa或bbb来验证
### 之后再clone项目就可以直接使用配置的别名
### 例如 `git clone git@aaa:xxxxx/xxxx.git`
### 如果是已经clone过的项目，可以切换远程仓库地址，进入.git目录，修改config文件中的地址即可
### 或者用git命令也可以(自行百度吧)
