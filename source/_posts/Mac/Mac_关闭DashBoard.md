title: 关闭DashBoard
date: 2016-01-22 09:12:42
categories:
- Mac
tags:
- DashBoard
---

# 关闭DashBoard

### 仪表盘，曾经一度是 OS X 的亮点之处，给前几代的 OS X 用户提供了很多便利，利用小插件（Widgets）来管理日常简单工作，非常方便。在多桌面管理界面（Mission Control）也能看见它的面孔。
### 然而如今的 OS X 有了更多其他的功能，更快的程序启动速度，完全不需要这个 Dashboard 来完成任务了，反而成了一种累赘。显然，仪表盘已经被苹果抛弃了，没有原来那样的实际用途，就连 UI 也和 Yosemite 的扁平界面违和不以。那作为用户也得表示表示，高冷地把它禁用掉，省着碍眼。

```
defaults write com.apple.dashboard mcx-disabled -boolean YES && killall Dock
```

### Dock 栏会消失并重新浮现，现在用触控板向右滑动，发现仪表盘的确消失了。
### 如果想再看到仪表盘请键入以下命令

```
defaults write com.apple.dashboard mcx-disabled -boolean NO && killall Dock
```
