title: 特殊字符
date: 2016-08-11 09:12:42
categories:
- Mac
tags:
- 特殊字符
---

# 特殊字符
`alt + 1` = `¡`
`alt + 2` = `™`
`alt + 3` = `£`
`alt + 4` = `¢`
`alt + 5` = `∞`
`alt + 6` = `§`
`alt + 7` = `¶`
`alt + 8` = `•`
`alt + 9` = `ª`
`alt + 0` = `º`
`alt + -` = `–`
`alt + =` = `≠`
`alt + a` = `å`
`alt + b` = `∫`
`alt + c` = `ç`
`alt + d` = `∂`
`alt + e` = `´`
`alt + f` = `ƒ`
`alt + g` = `©`
`alt + h` = `˙`
`alt + i` = `ˆ`
`alt + j` = `∆`
`alt + k` = `˚`
`alt + l` = `¬`
`alt + m` = `µ`
`alt + n` = `˜`
`alt + o` = `ø`
`alt + p` = `π`
`alt + q` = `œ`
`alt + r` = `®`
`alt + s` = `ß`
`alt + t` = `†`
`alt + u` = `¨`
`alt + v` = `√`
`alt + w` = `∑`
`alt + x` = `≈`
`alt + y` = `¥`
`alt + z` = `Ω`

![](http://7xqedu.com1.z0.glb.clouddn.com/YmxvZ18xNjA5MjAwMV9vcHRpb24=.jpg_s)
![](http://7xqedu.com1.z0.glb.clouddn.com/YmxvZ18xNjA5MjAwMV9zaGlmdC1vcHRpb24=.jpg_s)


