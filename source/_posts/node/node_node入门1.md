title: node入门1
date: 2016-06-04 09:12:42
categories:
- node
tags:
- node
---

# node入门1

### Node入门课程 1
### 记录一下自己的node学习历程。
### 很尴尬，最初安装node只是出于对hexo的兴趣，具体的安装过程已经想不起来，推荐CNode：Node.js专业中文社区,感兴趣的行查阅一下吧。
### 越过了安装呢，其实也没省多少事(-_-!)。好吧，该来点干货了。先上代码：
### 再插一嘴，关于JS基础的呢我就不说了，我的基础也不好，以后会涉及到的我再单独介绍。这段代码应该很好理解，我写了注释，大家可以参考一下：


```
var express = require('express'); //这一行代码的作用是引用外部express模块。
var app = express();

app.get('/', function (req, res) {
res.send('你好 世界');
});

app.listen(3000, function () {
console.log('app is listening at port 3000');
});
```
