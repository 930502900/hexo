title: React Nativeの坑
date: 2016-08-16 09:12:42
categories:
- React Native
tags:
- 坑
---

# React Nativeの坑

### 不会报错的RN与React配比

### For RN `0.30 and 0.31`: `npm install react@15.2.1`
### For RN `0.32`: `npm install react@15.3.0`

### RN->View支持的属性

```
width
height
top
left
right
bottom
minWidth
maxWidth
minHeight
maxHeight
margin
marginVertical
marginHorizontal
marginTop
marginBottom
marginLeft
marginRight
padding
paddingVertical
paddingHorizontal
paddingTop
paddingBottom
paddingLeft
paddingRight
borderWidth
borderTopWidth
borderRightWidth
borderBottomWidth
borderLeftWidth
position
flexDirection
flexWrap
justifyContent
alignItems
alignSelf
flex
zIndex
shadowColor
shadowOffset
shadowOpacity
shadowRadius
transform
transformMatrix
decomposedMatrix
scaleX
scaleY
rotation
translateX
translateY
backfaceVisibility
backgroundColor
borderColor
borderTopColor
borderRightColor
borderBottomColor
borderLeftColor
borderRadius
borderTopLeftRadius
borderTopRightRadius
borderBottomLeftRadius
borderBottomRightRadius
borderStyle
opacity
overflow
elevation
```

### RN->Text支持的属性

```
color
fontFamily
fontSize
fontStyle
fontWeight
textShadowOffset
textShadowRadius
textShadowColor
letterSpacing
lineHeight
textAlign
textAlignVertical
textDecorationLine
textDecorationStyle
textDecorationColor
writingDirection
```

### RN->TextInput

### 给text input设置一个`ref`,然后调用`blur()`即可使其失去焦点

### RN->jsbundle打包

```
react-native bundle --entry-file ./index.ios.js --bundle-output ./ios/main.jsbundle --platform ios --dev false --assets-dest ./ios/
```

